package main

import "embed"

// All embed variables used across the package

//go:embed all:templates
var templatesFS embed.FS

//go:embed all:static
var staticFS embed.FS
