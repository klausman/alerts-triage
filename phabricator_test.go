package main

import (
	"net/url"
	"strings"
	"testing"

	"github.com/prometheus/alertmanager/api/v2/models"
)

func testAlert(t *testing.T) (models.GettableAlert, error) {
	a := models.GettableAlert{}
	err := a.UnmarshalJSON([]byte(`
	{
        "annotations": {
          "dashboard": "TODO"
        },
        "endsAt": "2023-10-21T20:39:02.492Z",
        "fingerprint": "f732d582f08cbc05",
        "receivers": [ { "name": "logger" } ],
        "startsAt": "2023-10-05T14:09:02.492Z",
        "status": { "state": "active" },
        "updatedAt": "2023-10-21T20:35:02.697Z",
        "generatorURL": "https://prometheus-eqiad.wikimedia.org/ops/graph?g0.expr=pint_problem+%3E+0&g0.tab=1",
        "labels": {
          "alertname": "test",
          "job": "pint",
          "severity": "warning",
          "site": "eqiad",
          "team": "sre"
        }
      }
`))
	if err != nil {
		t.Errorf("Failed to create test alert %s", err)
	}
	return a, err
}

func testConfig(t *testing.T, extra string) (*Config, error) {
	c, err := Load(`
alert_dashboard_url: http://foo.com
phabricator_url: http://bar.com
alertmanager_url: http://zomg.org
` + extra)
	if err != nil {
		t.Errorf("Failed to create test config %s", err)
	}
	return c, nil
}

func values(raw ...string) *url.Values {
	u := &url.URL{}
	u.RawQuery = strings.Join(raw, "&")
	res := u.Query()
	return &res
}

/*
TestPhabricator_routes tests basic end-to-end functionality for building new task urls.
For given combinations of (route, alert) make sure the query string contains the values we're expecting.
*/
func TestPhabricator_routes(t *testing.T) {
	alert, _ := testAlert(t)
	singleFullRoute := `
phabricator_routes:
  - matchers:
      - alertname =~ test
    projects:
       - test-project
    priority: high
`
	noRoute := ""
	overrideRoute := `
phabricator_routes:
  - matchers:
      - alertname =~ .*
    projects:
       - all
    priority: medium
    continue: true
  - matchers:
      - alertname =~ test
    projects:
       - test-project
    priority: high
`
	tests := []struct {
		name    string
		routes  string
		alert   models.GettableAlert
		want    *url.Values
		wantErr bool
	}{
		{"noRoute", noRoute, alert, &url.Values{}, false},
		{"simpleRoute", singleFullRoute, alert, values("priority=high", "projects=test-project"), false},
		{"overrideRoute", overrideRoute, alert, values("priority=high", "projects=all,test-project"), false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, _ := testConfig(t, tt.routes)
			p, err := NewPhabricator(c)
			if err != nil {
				t.Errorf("Unable to create Phabricator %s", err)
			}

			got, err := p.NewTaskURL(tt.alert)
			if (err != nil) != tt.wantErr {
				t.Errorf("Phabricator.NewTaskURL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !strings.Contains(got.Query().Encode(), tt.want.Encode()) {
				t.Errorf("Phabricator.NewTaskURL() = %v, want %v", got.Query().Encode(), tt.want.Encode())
			}
		})
	}
}
