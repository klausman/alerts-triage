package main

import (
	"fmt"
	"net/url"
	"os"
	"sort"
	"strings"

	"github.com/prometheus/alertmanager/pkg/labels"
	"gopkg.in/yaml.v3"
)

// yamlURL is an alias to url.URL to support yaml unmarshaling.
type yamlURL struct {
	*url.URL
}

func (u *yamlURL) UnmarshalYAML(value *yaml.Node) error {
	var s string
	err := value.Decode(&s)
	if err != nil {
		return err
	}
	u.URL, err = url.ParseRequestURI(s)
	return err
}

// Config contains all information to run the application.
type Config struct {
	URLPrefix              string             `yaml:"url_prefix"`
	PhabricatorURL         *yamlURL           `yaml:"phabricator_url"`
	PhabricatorSubscribers []string           `yaml:"phabricator_subscribers"`
	PhabricatorRoutes      []PhabricatorRoute `yaml:"phabricator_routes"`
	AlertmanagerURL        *yamlURL           `yaml:"alertmanager_url"`
	AlertDashboardURL      *yamlURL           `yaml:"alert_dashboard_url"`
	RefreshAlertsSeconds   int                `yaml:"refresh_alerts_seconds"`
	StubAlertsPath         string             `yaml:"stub_alerts_path"`
	ManiphestID            int                `yaml:"maniphest_id"`
}

// Matchers is an alias to labels.Matchers to be able to UnmarshalYAML
type Matchers labels.Matchers

// UnmarshalYAML parses Matchers
func (m *Matchers) UnmarshalYAML(value *yaml.Node) error {
	var lines []string
	err := value.Decode(&lines)
	if err != nil {
		return err
	}

	for _, line := range lines {
		pm, err := labels.ParseMatchers(line)
		if err != nil {
			return err
		}
		*m = append(*m, pm...)
	}

	sort.Sort(labels.Matchers(*m))
	return nil

}

// Load parses and validates a configuration contained in s.
func Load(s string) (*Config, error) {
	cfg := &Config{}
	err := yaml.Unmarshal([]byte(s), cfg)
	if err != nil {
		return nil, err
	}

	if cfg.URLPrefix != "" && !strings.HasPrefix(cfg.URLPrefix, "/") {
		return nil, fmt.Errorf("url_prefix must start with /")
	}

	if cfg.RefreshAlertsSeconds == 0 {
		cfg.RefreshAlertsSeconds = 60
	}

	if cfg.ManiphestID == 0 {
		cfg.ManiphestID = 1
	}

	switch {
	case cfg.PhabricatorURL == nil || cfg.PhabricatorURL.String() == "":
		return nil, fmt.Errorf("'phabricator_url' missing")
	case cfg.AlertmanagerURL == nil || cfg.AlertmanagerURL.String() == "":
		return nil, fmt.Errorf("'alertmanager_url' missing")
	case cfg.AlertDashboardURL == nil || cfg.AlertDashboardURL.String() == "":
		return nil, fmt.Errorf("'alert_dashboard_url' missing")
	}

	return cfg, nil
}

// NewConfig loads configuration from a path.
func NewConfig(path string) (*Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cfg, err := Load(string(data))
	if err != nil {
		return nil, err
	}
	return cfg, nil
}
